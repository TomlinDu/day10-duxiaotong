package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private final EmployeeJPARepository employeeJPARepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, EmployeeJPARepository employeeJPARepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        List<Employee> employeeList = employeeJPARepository.findAll();
        return employeeList.stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public EmployeeResponse findById(Long id) {
        return EmployeeMapper.toResponse(employeeJPARepository.findById(id).get());
    }

    public EmployeeResponse update(Long id, EmployeeRequest employeeRequest) {
        Optional<Employee> employeeOptional = employeeJPARepository.findById(id);
        if (employeeOptional.isEmpty()){
            throw new EmployeeNotFoundException();
        }
        Employee toBeUpdatedEmployee = employeeOptional.get();
        if (employeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(employeeRequest.getAge());
        }
        Employee savedEmployee = employeeJPARepository.save(toBeUpdatedEmployee);
        return EmployeeMapper.toResponse(savedEmployee);
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        List<Employee> employeeList = employeeJPARepository.findByGender(gender);
        return employeeList.stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public Employee create(Employee employee) {
        return getEmployeeRepository().insert(employee);
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        List<Employee> employeeList = employeeJPARepository.findAll(pageRequest).getContent();
        return employeeList.stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }
}
