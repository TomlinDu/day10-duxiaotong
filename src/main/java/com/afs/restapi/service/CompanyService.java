package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final InMemoryCompanyRepository inMemoryCompanyRepository;
    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;
    private final CompanyJPARepository companyJPARepository;

    public CompanyService(InMemoryCompanyRepository inMemoryCompanyRepository, InMemoryEmployeeRepository inMemoryEmployeeRepository, CompanyJPARepository companyJPARepository) {
        this.inMemoryCompanyRepository = inMemoryCompanyRepository;
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.companyJPARepository = companyJPARepository;
    }

    public InMemoryCompanyRepository getCompanyRepository() {
        return inMemoryCompanyRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<CompanyResponse> findAll() {
        List<Company> companies = companyJPARepository.findAll();
        return companies.stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        List<Company> companies = companyJPARepository.findAll(pageRequest).getContent();
        return companies.stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
    }

    public CompanyResponse findById(Long id) {
        return CompanyMapper.toResponse(companyJPARepository.findById(id).get());
    }

    public CompanyResponse update(Long id, CompanyRequest companyRequest) {
        Optional<Company> companyOptional = companyJPARepository.findById(id);
        if (companyOptional.isEmpty()){
            throw new CompanyNotFoundException();
        }
        Company oldCompany = companyOptional.get();
        oldCompany.setName(companyRequest.getName());
        Company savedCompany = companyJPARepository.save(oldCompany);
        return CompanyMapper.toResponse(savedCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.toEntity(companyRequest);
        companyJPARepository.save(company);
        return CompanyMapper.toResponse(company);
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        List<Employee> employeeList = companyJPARepository.findById(id).map(Company::getEmployees).orElseThrow(CompanyNotFoundException::new);
        return employeeList.stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}
