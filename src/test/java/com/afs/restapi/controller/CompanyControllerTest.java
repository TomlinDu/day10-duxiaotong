package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private InMemoryCompanyRepository inMemoryCompanyRepository;
    @Autowired
    private InMemoryEmployeeRepository inMemoryEmployeeRepository;
    @Autowired
    private CompanyJPARepository companyJPARepository;
    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    @BeforeEach
    void setUp() {
        inMemoryCompanyRepository.clearAll();
        inMemoryEmployeeRepository.clearAll();
        companyJPARepository.deleteAll();
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_update_company_name() throws Exception {
        Company previousCompany = getCompany1();
        Company savedCompany = companyJPARepository.save(previousCompany);

        CompanyRequest companyUpdateRequest = new CompanyRequest("xyz");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateRequest);
        mockMvc.perform(put("/companies/{id}", savedCompany.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<Company> optionalCompany = companyJPARepository.findById(savedCompany.getId());
        assertTrue(optionalCompany.isPresent());
        Company updatedCompany = optionalCompany.get();
        Assertions.assertEquals(previousCompany.getId(), updatedCompany.getId());
        Assertions.assertEquals(companyUpdateRequest.getName(), updatedCompany.getName());
    }

    @Test
    void should_delete_company_name() throws Exception {
        CompanyRequest companyRequest = getCompanyResquest1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(companyRequest));

        mockMvc.perform(delete("/companies/{id}", savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(inMemoryCompanyRepository.findById(savedCompany.getId()).isEmpty());
    }

    @Test
    void should_delete_employees_in_company_when_delete_company_given_company_id() throws Exception {
        CompanyRequest companyRequest = getCompanyResquest1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(companyRequest));
        Employee employee1 = new Employee(null, "zhangsan", 22, "Male", 1000, savedCompany.getId());
        Employee savedEmployee1 = employeeJPARepository.save(employee1);

        mockMvc.perform(delete("/companies/{id}", savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(companyJPARepository.findById(savedCompany.getId()).isEmpty());
        assertTrue(employeeJPARepository.findById(savedEmployee1.getId()).isEmpty());
    }


    @Test
    void should_create_company() throws Exception {
        CompanyRequest companyRequest1 = getCompanyResquest1();

        ObjectMapper objectMapper = new ObjectMapper();
        String companyRequest = objectMapper.writeValueAsString(companyRequest1);
        mockMvc.perform(post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companyRequest1.getName()));
    }

    @Test
    void should_find_companies() throws Exception {
        CompanyRequest companyRequest1 = getCompanyResquest1();
        Company saveCompany = companyJPARepository.save(CompanyMapper.toEntity(companyRequest1));

        mockMvc.perform(get("/companies"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(saveCompany.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(companyRequest1.getName()));
    }

    @Test
    void should_find_companies_by_page() throws Exception {
        CompanyRequest companyRequest1 = getCompanyResquest1();
        CompanyRequest companyRequest2 = getCompanyResquest2();
        CompanyRequest companyRequest3 = getCompanyResquest3();
        Company saveCompany1 = companyJPARepository.save(CompanyMapper.toEntity(companyRequest1));
        Company saveCompany2 = companyJPARepository.save(CompanyMapper.toEntity(companyRequest2));
        Company saveCompany3 = companyJPARepository.save(CompanyMapper.toEntity(companyRequest3));

        mockMvc.perform(get("/companies")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(saveCompany1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(companyRequest1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(saveCompany2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(companyRequest2.getName()))
        ;
    }

    @Test
    void should_find_company_by_id() throws Exception {
        CompanyRequest companyRequest1 = getCompanyResquest1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(companyRequest1));

        mockMvc.perform(get("/companies/{id}", savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companyRequest1.getName()));
    }

    @Test
    void should_find_employees_by_companies() throws Exception {
        CompanyRequest companyRequest1 = getCompanyResquest1();
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(companyRequest1));
        Employee employee = getEmployee(savedCompany);
        Employee savedEmployee = employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies/{companyId}/employees", savedCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(savedEmployee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(savedEmployee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(savedEmployee.getGender()));
    }

    private static Employee getEmployee(Company company) {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        employee.setCompanyId(company.getId());
        return employee;
    }


    private static Company getCompany1() {
        Company company = new Company();
        company.setName("ABC");
        return company;
    }

    private CompanyRequest getCompanyResquest1() {
        return new CompanyRequest("ABC");
    }

    private static Company getCompany2() {
        Company company = new Company();
        company.setName("DEF");
        return company;
    }
    private CompanyRequest getCompanyResquest2() {
        return new CompanyRequest("DEF");
    }

    private static Company getCompany3() {
        Company company = new Company();
        company.setName("XYZ");
        return company;
    }
    private CompanyRequest getCompanyResquest3() {
        return new CompanyRequest("XYZ");
    }

}