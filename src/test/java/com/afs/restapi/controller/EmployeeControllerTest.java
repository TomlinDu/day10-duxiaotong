package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.mapper.EmployeeMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private InMemoryEmployeeRepository inMemoryEmployeeRepository;

    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    @BeforeEach
    void setUp() {
        inMemoryEmployeeRepository.clearAll();
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_update_employee_age_and_salary() throws Exception {
        Company company = new Company(null, "oocl");
        Employee previousEmployee = new Employee(null, "zhangsan", 22, "Male", 1000, company.getId());
        Employee savedEmployee = employeeJPARepository.save(previousEmployee);

        EmployeeRequest employeeUpdateRequest = new EmployeeRequest("zhangsan", 24, "Male", 2000, company.getId());
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(employeeUpdateRequest);
        mockMvc.perform(put("/employees/{id}", savedEmployee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<Employee> optionalEmployee = employeeJPARepository.findById(savedEmployee.getId());
        assertTrue(optionalEmployee.isPresent());
        Employee updatedEmployee = optionalEmployee.get();
        Assertions.assertEquals(employeeUpdateRequest.getAge(), updatedEmployee.getAge());
        Assertions.assertEquals(employeeUpdateRequest.getSalary(), updatedEmployee.getSalary());
        Assertions.assertEquals(previousEmployee.getId(), updatedEmployee.getId());
        Assertions.assertEquals(previousEmployee.getName(), updatedEmployee.getName());
        Assertions.assertEquals(previousEmployee.getGender(), updatedEmployee.getGender());
    }

    @Test
    void should_create_employee() throws Exception {
        EmployeeRequest request = getEmployeeRequestZhangsan();
        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequest = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(request.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(request.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(request.getGender()))
        ;
    }

    @Test
    void should_find_employees() throws Exception {
        EmployeeRequest request = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(request));

        mockMvc.perform(get("/employees"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(request.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(request.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(request.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").value(request.getCompanyId()));
    }

    @Test
    void should_find_employee_by_id() throws Exception {
        EmployeeRequest request = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(request));

        mockMvc.perform(get("/employees/{id}", savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(savedEmployee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(savedEmployee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(savedEmployee.getGender()));
    }

    @Test
    void should_delete_employee_by_id() throws Exception {
        EmployeeRequest request = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(request));

        mockMvc.perform(delete("/employees/{id}", savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(employeeJPARepository.findById(savedEmployee.getId()).isEmpty());
    }

    @Test
    void should_find_employee_by_gender() throws Exception {
        EmployeeRequest request = getEmployeeRequestZhangsan();
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(request));

        mockMvc.perform(get("/employees?gender={0}", "Male"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(request.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(request.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(request.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").value(request.getCompanyId()));
    }

    @Test
    void should_find_employees_by_page() throws Exception {
        EmployeeRequest request1 = getEmployeeRequestZhangsan();
        Employee savedEmployee1 = employeeJPARepository.save(EmployeeMapper.toEntity(request1));
        EmployeeRequest request2 = getEmployeeRequestSusan();
        Employee savedEmployee2 = employeeJPARepository.save(EmployeeMapper.toEntity(request2));
        EmployeeRequest request3 = getEmployeeRequestLisi();
        Employee savedEmployee3 = employeeJPARepository.save(EmployeeMapper.toEntity(request3));

        mockMvc.perform(get("/employees")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedEmployee1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(request1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(request1.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(request1.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(savedEmployee2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(request2.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(request2.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value(request2.getGender()));
    }

    private static EmployeeRequest getEmployeeRequestZhangsan() {
        EmployeeRequest request = new EmployeeRequest();
        request.setName("zhangsan");
        request.setAge(22);
        request.setGender("Male");
        request.setSalary(10000);
        return request;
    }

    private static Employee getEmployeeZhangsan() {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        return employee;
    }

    private static EmployeeRequest getEmployeeRequestSusan() {
        EmployeeRequest request = new EmployeeRequest();
        request.setName("susan");
        request.setAge(23);
        request.setGender("Male");
        request.setSalary(11000);
        return request;
    }
    private static Employee getEmployeeSusan() {
        Employee employee = new Employee();
        employee.setName("susan");
        employee.setAge(23);
        employee.setGender("Male");
        employee.setSalary(11000);
        return employee;
    }

    private static EmployeeRequest getEmployeeRequestLisi() {
        EmployeeRequest request = new EmployeeRequest();
        request.setName("lisi");
        request.setAge(24);
        request.setGender("Female");
        request.setSalary(12000);
        return request;
    }
    private static Employee getEmployeeLisi() {
        Employee employee = new Employee();
        employee.setName("lisi");
        employee.setAge(24);
        employee.setGender("Female");
        employee.setSalary(12000);
        return employee;
    }
}